[![pipeline status](https://gitlab.com/nicosingh/rpi-dind/badges/master/pipeline.svg)](https://gitlab.com/nicosingh/rpi-dind/commits/master) [![Docker Pulls](https://img.shields.io/docker/pulls/nicosingh/rpi-dind.svg)](https://hub.docker.com/r/nicosingh/rpi-dind/)

# About

This raspbian-based image allows you to run docker commands inside a Docker container. It contains the [official installation of Docker CE](https://docs.docker.com/engine/installation/linux/docker-ce/debian/) in a raspbian image (Jessie built on 14 Feb 2018). It is useful to test isolated docker commands, or run Docker-based tools inside a running container (e.g. gitlab runners).

# How to run this image?

`$ docker run -it --rm -v /var/run/docker.sock:/var/run/docker.sock nicosingh/rpi-dind bash`

Where:

`-it` (optional): if we want to execute an interactive container. Use `-d` if we want to run this image in detached mode.

`--rm` (optional): if we want to remove the container once it finishes.

`-v /var/run/docker.sock:/var/run/docker.sock` (optional): if we want to link our local docker socket to this container's one. It's not required if we are going to manually start docker service from the new image (in this case, using `--privileged` option is a good idea).

`nicosingh/rpi-dind`: image name.

`bash` (optional): the command we want to execute. For example, open a Bash terminal.
