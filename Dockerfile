FROM resin/rpi-raspbian:stretch-20180214

MAINTAINER Nicolas Singh <nicolas.singh@gmail.com>

# download required software
RUN apt-get update -y && \
  apt-get -y install apt-transport-https ca-certificates curl gnupg2 software-properties-common && \
  rm -rf /var/lib/apt/lists/*

# add docker key
RUN curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -

# add docker repository
RUN echo "deb [arch=armhf] https://download.docker.com/linux/debian \
     $(lsb_release -cs) stable" | \
    sudo tee /etc/apt/sources.list.d/docker.list

# install docker
RUN apt-get update && \
  apt-get -y install docker-ce && \
  rm -rf /var/lib/apt/lists/*
